import { ActionTypes } from "../constants/action-types"

export const setUsers = (users) => {
    return {
        type : ActionTypes.SET_USERS,
        payload : users
    };
};
export const editUser = (user) => {
    return {
        type : ActionTypes.EDIT_USER,
        payload : user
    };
};
export const addUser = (user) => {
    return {
        type : ActionTypes.ADD_USER,
        payload : user
    };
};
export const selectedUser = (user) => {
    return {
        type : ActionTypes.SELECTED_USER,
        payload : user
    };
};

export const removeSelectedUser = () => {
    return {
        type : ActionTypes.REMOVE_SELECTED_USER,
    };
};
import { ActionTypes } from "../constants/action-types";

const initialState = {
    users: [],
    loading: true
}

export const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.SET_USERS:
            return {
                ...state,
                users: payload,
                loading: false
            };


        case ActionTypes.USERS_ERROR:
            return {
                loading: false,
                error: payload
            }
        default:
            return state;
    };
};

export const selectedUserReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.SELECTED_USER:
            return {
                ...state,
                ...payload,
                loading: false
            };
        case ActionTypes.EDIT_USER:

            return {
                ...state,
                ...payload,
                loading: false,

            };
        case ActionTypes.ADD_USER:
            return {
                ...state,
                ...payload,
                loading: false
            };
        case ActionTypes.USERS_ERROR:
            return {
                loading: false,
                error: payload
            };

        case ActionTypes.REMOVE_SELECTED_USER:
            return {};

        default:
            return state;
    };
};
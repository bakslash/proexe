import axios from 'axios'

const URL = "https://my-json-server.typicode.com/karolkproexe/jsonplaceholderdb/data"

export const getUsers = () =>{
    return axios.get(URL,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        })
}

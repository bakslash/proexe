import React from "react";
import { useSelector } from "react-redux";
import { Link } from 'react-router-dom'

const Users = () => {
    const users = useSelector(state => state.allUsers.users);
    if (users != null) {
        console.log('from users',users);
        const renderList = users.map((user) => {
            const { id, name, username, email,address } = user;
            return (
                <tr>
                    <td className=" bg-white text-sm">
                        {id}
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                        {name}
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                        {username}
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                        {email}
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                        {address.city}
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                     <Link to={{ pathname: `users/${id}` }} >
                        <button className="bg-orange-400 text-white px-4 py-1 rounded">
                            Edit
                        </button>
                   </Link>
                    </td>
                    <td className="px-5 py-3  bg-white text-sm">
                        <button className="bg-red-700 text-white px-4 py-1 rounded">
                            Delete
                        </button>
                    </td>
                </tr>
            );

        })
        return (
            <>
                <div className="bg-white p-8 rounded-md w-full ">

                    <div className=" w-full">
                        <h2 className="text-gray-600 font-semibold mb-2">Dashboard</h2>
                    </div>

                    <div className=" flex  justify-between pb-2 ">

                        <div className="w-full py-2 px-2">
                            <span className="text float-left font-semibold">User List</span>
                            <span className="text float-right mr-24">
                            <Link to={{ pathname: `users/add` }} >
                                <button className="bg-blue-500 text-white px-6 py-1 rounded">
                                    Add
                                </button>
                                </Link>
                            </span>
                        </div>
                    </div>

                    <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto ">
                        <div className="inline-block min-w-full shadow rounded-lg overflow-hidden ">
                            <table className="min-w-full leading-normal">
                                <thead>
                                    <tr className=" bg-blue-100">
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            Id
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            name
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            username
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            email
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            city
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            edit
                                        </th>
                                        <th
                                            className="px-5 py-3  text-xs font-semibold text-gray-600 uppercase ">
                                            delete
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {renderList}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </>
        );

    };
};

export default Users;
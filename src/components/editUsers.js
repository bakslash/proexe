import React, { useState, useEffect } from 'react'
import { editUser } from '../actions/userActions'
import { useSelector, useDispatch } from 'react-redux'
import { NavLink, useParams ,useNavigate } from 'react-router-dom'
import { ActionTypes } from '../constants/action-types'

export default function EditUser() {
    let navigate = useNavigate()
    const dispatch = useDispatch()
    const [name, setName] = React.useState("");
    const [username, setUserName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [city, setCity] = React.useState("");
    const { id } = useParams()

    const users = useSelector(state => state.allUsers.users);
    let found_user = users.find(user => { return user.id == id  })
    
    
        useEffect( () => {  
            setName(found_user.name);
            setEmail(found_user.email);
            setUserName(found_user.username);
            setCity(found_user.address.city);
       }, []) 

    const handleName = (e) => {
        setName(e.target.value);
        
    }
    const handleEmail = (e) => {
        setEmail(e.target.value);
    }
    const handleUserName = (e) => {
        setUserName(e.target.value);
    }
    const handleCity = (e) => {
        setCity(e.target.value);

    }
    const handleEdit = (e) => {
       const upDated = {id,name,username,email,city}
        dispatch(editUser(upDated))
        navigate('/')
          
    }
    return (
        <div className="bg-white p-8 rounded-md w-full ">
            <div className=" flex items-center justify-between pb-6">
                <div>
                    <h2 className="text-gray-600 font-semibold">Edit User</h2>
                    <span className="text-xs">User Details</span>
                </div>
            </div>
            <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto ">
                <div className="inline-block min-w-full shadow rounded-lg overflow-hidden ">
                    <table className="min-w-full leading-normal">
                        <thead>
                            <tr className=" bg-blue-100">
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Id
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Name
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200  text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Email
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200  text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Username
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200  text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    City
                                </th>
                                <th
                                    className="px-5 py-3 border-b-2 border-gray-200  text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="px-5 py-5  border-gray-200 bg-white text-m">
                                    {id}
                                </td>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-m">
                                    <input type="text" className="border border-black rounded"
                                        onChange={handleName}
                                        name='name'
                                        value={name} />
                                </td>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-m">
                                    <input type="text" className="border border-black rounded"
                                        onChange={handleEmail}
                                        name='email'
                                        value={email} />
                                </td>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-m">
                                    <input type="text" className="border border-black rounded"
                                        onChange={handleUserName}
                                        name='username'
                                        value={username} />
                                </td>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-m">
                                    <input type="text" className="border border-black rounded"
                                        onChange={handleCity}
                                        name='city'
                                        value={city} />
                                </td>
                                <td className="px-5 py-5 border-b border-gray-200 bg-white text-m">
                                    <button className="bg-purple-500 text-white px-4 py-1 rounded"
                                        onClick={handleEdit}>
                                        Save
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
import React from 'react'
import { addUser } from '../actions/userActions'
import { useSelector, useDispatch } from 'react-redux'
import { NavLink ,useNavigate } from 'react-router-dom'
import { ActionTypes } from '../constants/action-types'


export default function AddUsers() {
    let navigate = useNavigate()
    const dispatch = useDispatch()
    const [name, setName] = React.useState("");
    const [username, setUserName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [city, setCity] = React.useState("");

    const handleName = (e) => {
        setName(e.target.value);
        console.log(name);
    }
    const handleEmail = (e) => {
        setEmail(e.target.value);
    }
    const handleUserName = (e) => {
        setUserName(e.target.value);
    }
    const handleCity = (e) => {
        setCity(e.target.value);
        console.log(city);

    }
    const handleAdd =(e)=>{
    const userDetails = [name,username,email,city]

        dispatch(addUser(userDetails))
        navigate('/')
    }

    return (


        <div className="bg-white p-8 rounded-md w-2/3 border mt-20 ml-48 ">
            <div className=" flex items-center justify-between pb-6">
                <div>
                    <h2 className="text-gray-600 font-semibold">Add User</h2>
                    <span className="text-xs">User Details</span>
                </div>
            </div>
            
            <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto ">
                <div className="inline-block min-w-full shadow rounded-lg overflow-hidden ">
                <form>
                    <div className=" mb-2 p-2">
                        <label className="p-8">
                            Name
                        </label>
                        <input type="text"
                         className="border border-black  rounded w-2/3 "
                         onChange={handleName}
                                        name='name'
                                        value={name} />
                    </div>
                    <div className=" mb-2 p-2">
                        <label className="p-8">
                            Email
                        </label>
                        <input type="text" className="border border-black rounded  w-2/3" onChange={handleEmail}
                                        name='email'
                                        value={email} />
                    </div>
                    <div className=" mb-2 p-2 ">
                        <label className="px-2 ">
                            Username
                        </label>
                        <input type="text" className="border border-black w-2/3 rounded  mx-4"onChange={handleUserName}
                                        name='username'
                                        value={username} />
                    </div>
                    <div className=" mb-2 p-2">
                        <label className="px-8 ">
                            City
                        </label>
                        <input type="text" className="border border-black w-2/3 rounded mx-4" onChange={handleCity}
                                        name='name'
                                        value={city}/>
                    </div>
                    <div className=" m-2 p-2">
                    <button className="bg-blue-500 text-white px-6 py-1 rounded ml-28 mb-8 mt-8" onClick={handleAdd}>
                                    Add
                                </button>
                                </div>
                </form>
            </div>
        </div>
      </div >
      
        
  )
}

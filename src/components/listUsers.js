import React, { useState, useEffect } from 'react'
import { getUsers } from '../apis/services'
import { setUsers } from '../actions/userActions'
import { useSelector ,useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { ActionTypes } from '../constants/action-types'
import Users from './users'
import { Link } from 'react-router-dom'
export default function UserList() {
  const users = useSelector(state => state);
  const dispatch = useDispatch()

  const fetchUsers = async () => {
    const response = await getUsers()
    .catch((err) => {
        dispatch( {
            type: ActionTypes.USERS_ERROR,
            payload: err,
        })
        console.log("Err", err);
    });
    dispatch(setUsers(response.data));
    console.log(response.data);
};

useEffect(() => {
    fetchUsers();
}, []);
console.log(users);
  
  return (
    <>
   <Users />
    </>

  )
}

import React from 'react';
import { Route, Routes,  BrowserRouter, Navigate } from 'react-router-dom';
import './App.css';

import UserList from './components/listUsers';
import  EditUser  from './components/editUsers';
import  AddUsers  from './components/addUsers';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        
          <Route path="/" element={<UserList />} />
          <Route path="/users/:id" element={<EditUser />} />
          <Route path="/users/add" element={<AddUsers />} />

      

      </Routes>

    </BrowserRouter>

  );
}

export default App;
